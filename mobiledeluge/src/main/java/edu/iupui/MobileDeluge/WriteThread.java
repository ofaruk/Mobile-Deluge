package edu.iupui.MobileDeluge;

import android.os.*;
import android.os.Message;
import android.util.SparseArray;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import edu.iupui.MobileDeluge.Activity.MainActivity;

import static xdroid.toaster.Toaster.toast;

/**
 * Created by Omor on 10/03/2017.
 */

public class WriteThread extends HandlerThread {

    public static final int MSG_FROM_TESTDELUGE = 2;
    public static final int MSG_FROM_SERIALCOMM_NODEREPORT = 4;
    public static final int MSG_FROM_SERIALCOMM_NODEREADY = 7;
    public static final int MSG_FROM_SERIALCOMM_NODEABORT = 8;
    public static final int MSG_FROM_SERIALCOMM_NODEREPORT_SUBSET = 9;

    final double REPROGRAMMING_THRESHOLD_VOLTAGE = 2.7;

    //Deluge command constants
    private final int DISSEMINATION = 1;
    public static final int STOP_DISSEMINATION = 3;

    //Hashmap to store node and their corresponding voltages
    private SparseArray<Float> nodeToVoltage = new SparseArray<>();
    //Handler that is responsible to post message to the UI Thread
    private WeakReference<Handler> uiHandler;
    // Handler that is responsible to post messege to this thread itself
    public static Handler localHandler;

    TestDeluge testDeluge;
    SerialComm serialComm;

    //constructor that requires a Handler from the UI thread
    public WriteThread(Handler uiHandler) {
        super("Write Thread");
        this.uiHandler = new WeakReference<>(uiHandler);
    }

    //Prepare the Handler of this Thread. Must be called after the thread has been started
    public void initializeHandler() {
        localHandler = new HandleDelugeCommnads(getLooper());
    }

    /**
     * Method that is called from the MainActvity or UI thread to run the Deluge command by
     * Handler Thread.
     * @param moteIF from USBService to access low level ByteSource
     * @param command Deluge command
     * @param imageNumber Depending on the mote possible value can be 0,1 or 2
     */
    public void delugeDisseminationAndReboot(MoteIF moteIF, int command, int imageNumber) {
        checkHandler();
        Message msg = Message.obtain(localHandler,DISSEMINATION,command,imageNumber,moteIF);
        localHandler.sendMessage(msg);
    }

    public void delugeStopDissemination(MoteIF moteIF) {
        checkHandler();
        Message msg = Message.obtain(localHandler,STOP_DISSEMINATION, moteIF);
        localHandler.sendMessage(msg);
    }

    /**
     * Returns handler to this thread.
     * @return
     */
    public static Handler getWriteThreadHandler() {
        return localHandler;
    }



    private class HandleDelugeCommnads extends Handler {

        private HandleDelugeCommnads(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case DISSEMINATION: {
                    int command = msg.arg1;
                    int imageNum = msg.arg2;
                    MoteIF moteIF = (MoteIF) msg.obj;
                    sendDissCommand(moteIF,command,imageNum);
                    break;
                }
                //this message is from TestDeluge received method callback
                case MSG_FROM_TESTDELUGE: {
                    sendMessageToUIThread(msg.arg1);
                    break;
                }
                case STOP_DISSEMINATION: {
                    MoteIF moteIF = (MoteIF) msg.obj;
                    sendStopCommand(moteIF);
                    break;
                }

                case MSG_FROM_SERIALCOMM_NODEREPORT: {
                    //create a hashmap and record node voltages. voltages less than threshold are not added
                    decodeAndStoreMessageFromSerialComm(msg);
                    //System.out.println(nodeToVoltage.toString());
                    sendSerialCommMsgToUI(SerialComm.MSG_DETECT_NEIGHBOR,msg.obj, msg.getData());
                    break;
                }

                case MSG_FROM_SERIALCOMM_NODEREPORT_SUBSET: {
                    //create a hashmap and record node voltages. voltages less than threshold are not added
                    decodeAndStoreMessageFromSerialComm(msg);
                    //System.out.println(nodeToVoltage.toString());
                    sendSerialCommMsgToUI(SerialComm.MSG_DETECT_NEIGHBOR_SUBSET,msg.obj, msg.getData());
                    break;
                }

                case MSG_FROM_SERIALCOMM_NODEREADY: {
                    decodeAndStoreMessageFromSerialComm(msg);
                    sendSerialCommMsgToUI(SerialComm.MSG_NODE_READY,msg.obj,msg.getData());
                    break;
                }

                case MSG_FROM_SERIALCOMM_NODEABORT: {
                    sendSerialCommMsgToUI(Constants.NODE_ABORTED,msg.obj,msg.getData());
                    break;
                }

                default:
                    toast("Unknown command received in Handle Message (Write Thread)");
                    break;
            }
        }
    }

    /**
     * this method is run by the Handler Thread. Actually executes the deluge command
     * @param moteIF
     * @param command
     * @param imageNum
     */
    private void sendDissCommand(MoteIF moteIF, int command, int imageNum) {
        testDeluge = new TestDeluge(moteIF,command,imageNum);
        testDeluge.dissAndReprogram();
    }

    private void sendStopCommand(MoteIF moteIF) {
        testDeluge = new TestDeluge(moteIF,DelugeParams.DM_COMMAND,0);
        testDeluge.stopDissemination();
    }

    public void sendMobileDelugeDetectCommand(final MoteIF moteIF, final short cmd, final short targetCount, final int[] nodeList, final short channel) {
        Runnable commnad = new Runnable() {
            @Override
            public void run() {
                nodeToVoltage.clear();
                if (serialComm == null) {
                    serialComm = new SerialComm(moteIF, localHandler);
                }
                serialComm.sendCmd(cmd,targetCount,nodeList,channel);
            }
        };
        localHandler.post(commnad);
    }

    public void sendMobileDelugeDetectSubsetCommand(final MoteIF moteIF, final short cmd, final short targetCount, final int[] nodeList, final short channel) {
        Runnable commnad = new Runnable() {
            @Override
            public void run() {
                nodeToVoltage.clear();
                if (serialComm == null) {
                    serialComm = new SerialComm(moteIF, localHandler);
                }
                serialComm.sendCmd(cmd,targetCount,nodeList,channel);
            }
        };
        localHandler.post(commnad);
    }

    public void sendVoltageFilteredConnectCommand(MoteIF moteIF, short cmd, short targetCount, int[] nodeList, short channel) throws InterruptedException {
        //do check whether we have all the nodes in record before proceeding
            int newTargetCount = 0;
            int[] newNodeList = new int[nodeList.length];
            ArrayList<Integer> goodNode = new ArrayList<>();
            for (int i = 0; i < nodeList.length; i++) {
                if (nodeToVoltage.get(nodeList[i], (float) -1.0) > 0) {
                    if (nodeToVoltage.get(nodeList[i]) >= 2.7) {
                        goodNode.add(nodeList[i]);
                        newTargetCount++;
                    }
                }
            }
            newNodeList = new int[newTargetCount];
            Iterator<Integer> iterator = goodNode.iterator();
            for(int i=0;i<newTargetCount;i++) {
                newNodeList[i] = iterator.next().intValue();
            }
            //now call the command with new target
            //System.out.println(newNodeList.toString());
            //TimeUnit.SECONDS.sleep(5);
            sendMobileDelugeConnectCommand(moteIF,cmd,(short)newTargetCount,newNodeList,channel);
    }

    public void sendMobileDelugeConnectCommand(final MoteIF moteIF, final short cmd, final short targetCount, final int[] nodeList, final short channel) {
        Runnable commnad = new Runnable() {
            @Override
            public void run() {
                if (serialComm == null) {
                serialComm = new SerialComm(moteIF,localHandler);
                }
                serialComm.sendCmd(cmd,targetCount,nodeList,channel);
            }
        };
        localHandler.post(commnad);
    }

    public void sendMobileDelugeAbortCommand(final MoteIF moteIF, final short cmd, final short targetCount, final int[] nodeList, final short channel) {
        Runnable commnad = new Runnable() {
            @Override
            public void run() {
                if (serialComm == null) {
                serialComm = new SerialComm(moteIF,localHandler);
                }
                serialComm.sendCmd(cmd,targetCount,nodeList,channel);
            }
        };
        localHandler.post(commnad);
    }

    /**
     * sends notification to the UI thread when the response is received
     * @param status defines whether the send operation to hardware was successful
     */
    private void sendMessageToUIThread(int status) {
        if(nullCheckUIHandler()) {
            Message msg = uiHandler.get().obtainMessage(MainActivity.DISS_COMMAND_SENT,status,0);
            uiHandler.get().sendMessage(msg);
        }
    }

    /**
     * sends a copy of the received message from SerialComm to UI
     * for printing
     * @param what orginial value of received what
     * @param msg original message received as object
     */
    private void sendSerialCommMsgToUI(int what, Object msg, Bundle bundle) {
        Message msgToSend = uiHandler.get().obtainMessage(what,msg);
        msgToSend.setData(bundle);
        uiHandler.get().sendMessage(msgToSend);
    }

    private void decodeAndStoreMessageFromSerialComm(Message msg) {
        SerialReplyMsg detectMsg = (SerialReplyMsg) msg.obj;
        nodeToVoltage.put(detectMsg.get_nodeid(),msg.getData().getFloat("voltage"));
    }



    private boolean nullCheckUIHandler() {
        return uiHandler.get() != null;
    }

    private void checkHandler(){
        if (localHandler == null) {
            localHandler = new HandleDelugeCommnads(getLooper());
        }
    }

}
