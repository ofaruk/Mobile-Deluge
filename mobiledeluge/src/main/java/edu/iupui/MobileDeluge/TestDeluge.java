package edu.iupui.MobileDeluge;


import android.os.Handler;

import static xdroid.toaster.Toaster.toast;

/**
 * Created by ofaruk on 10/13/16.
 */

public class TestDeluge implements MessageListener {

    private MoteIF moteIF;
    private boolean ackReceived = false;
    //command is sent to nodeID 0
    private short nodeID = 0;
    private int mImageNumber;
    private Handler writeThread;

    public TestDeluge(MoteIF moteif, int command, int imageNumber) {
        this.moteIF = moteif;
        this.mImageNumber = imageNumber;
        if (command == DelugeParams.DM_COMMAND) {
            this.moteIF.registerListener(new DMReplyMsg(), this);
        }
        else
            this.moteIF.registerListener(new FMReplyMsg(),this);
    }

    //method to pass the reference to facilitate callback



    public void sendDMCommand(short command, short imgNum) {
        DMReqMsg dmReqMsg = new DMReqMsg();
        dmReqMsg.set_cmd(command);
        dmReqMsg.set_imgNum(imgNum);

        //System.out.println("Sending DMCommand");
        /*try {
            moteIF.send(nodeID,dmReqMsg);
        }
        catch (IOException e) {
            System.err.println("Exception while sending the packet");
            System.err.println(e);
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException exception) {
        }*/
        send(dmReqMsg);

    }

    //Deluge Manager command implementations

    public void stopDissemination() {
        if(!ackReceived) {
            //System.out.println("Sending Stop Dissemination Command");
            DMReqMsg deluge_cmd = new DMReqMsg();
            deluge_cmd.set_cmd(DelugeParams.DM_CMD_STOP);
            deluge_cmd.set_imgNum((short) 0);
            send(deluge_cmd);
        }
    }

    public void dissAndReprogram() {
        if (!ackReceived) {
            //System.out.println("Sending Dissemination and Reprogram Command");
            DMReqMsg deluge_cmd = new DMReqMsg();
            deluge_cmd.set_cmd(DelugeParams.DM_CMD_DISSEMINATE_AND_REPROGRAM);
            deluge_cmd.set_imgNum((short) mImageNumber);
            send(deluge_cmd);
        }
    }

    public void sendFMCommand() {
        if (!ackReceived) {
            short[] blank = {};
            FMReqMsg fmReqMsg = new FMReqMsg();
            fmReqMsg.set_cmd((short) DelugeParams.FM_CMD_IDENT);
            fmReqMsg.set_imgNum((short) 0);
            fmReqMsg.set_offset(0);
            fmReqMsg.set_len(0);
            //fmReqMsg.set_data(blank);

            //System.out.println("Sending FMCommand");
            send(fmReqMsg);
            /*try {
                moteIF.send(0, fmReqMsg);
            } catch (IOException e) {
                System.err.println("Exception while sending the packet");
                System.err.println(e);
            }*/
        /*try {
            Thread.sleep(1000);
        } catch (InterruptedException exception) {
        }*/
        }

    }

    public void messageReceived(int to, Message message) {

        ackReceived = true;
        switch (message.amType()) {
            case DelugeParams.DM_AMID: {
                DMReplyMsg dReplyMsg = (DMReplyMsg) message;
                if (dReplyMsg.get_error() == DelugeParams.ERROR_SUCCESS) {
                    //System.out.println("DM Command sent successfully");
                    sendNotification(1); // int 1 resembles successfully sent command to hardware
                    //toast("Command Sent");
                } else {
                    //System.out.println("DM Command sent but did not receive reply");
                    sendNotification(0);
                }
                this.moteIF.deregisterListener(new DMReplyMsg(), this);
                break;
            }

            case DelugeParams.FM_AMID: {
                FMReplyMsg fReplyMsg = (FMReplyMsg) message;
                if (fReplyMsg.get_error() == DelugeParams.ERROR_SUCCESS) {
                    //System.out.println("FM Command sent successfully");
                    //toast("Command Sent");
                } else {
                    //System.out.println("FM Command sent but did not receive reply");
                }
                this.moteIF.deregisterListener(new FMReplyMsg(), this);
                break;
            }

            default: {
                //System.out.println((int) message.amType());
                //System.out.println("Unknown AM type ");
                break;
            }
        }
        //unregisterListeners();
        //System.out.println(message.amType());
        //unregisterListeners();

    }

    synchronized private void send(Message m) {
        try {
            //moteif.send(MoteIF.TOS_BCAST_ADDR, m);
            moteIF.send(this.nodeID, m);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void unregisterListeners() {
        this.moteIF.deregisterListener(new FMReplyMsg(),this);
    }

    /**
     * helper method that sends notification to the Writethread
     * @param input_status
     */
    void sendNotification(int input_status) {

        if (writeThread == null) {
            writeThread = WriteThread.getWriteThreadHandler();
        }
        //obtain a message to send to writethread looper arg2 is set to 0 but never used
        android.os.Message msg = writeThread.obtainMessage(WriteThread.MSG_FROM_TESTDELUGE,input_status,0);
        writeThread.sendMessage(msg);
    }

}
