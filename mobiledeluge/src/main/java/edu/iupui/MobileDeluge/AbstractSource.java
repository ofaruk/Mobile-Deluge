package edu.iupui.MobileDeluge;

/**
 * Created by Omor on 4/09/2016.
 */
import java.io.*;

//import net.tinyos.message.*;

/**
 * Provide a standard, generic implementation of PacketSource. Subclasses
 * need only implement low-level open and close operations, and packet
 * reading and writing. This class provides the automatic close-on-error
 * functionality, general error checking, and standard messages.
 */
abstract public class AbstractSource implements PacketSource
{
    protected String name;
    protected boolean opened = false;
    protected Messenger messages;

    protected void message(String s) {
        if (messages != null)
            messages.message(s);
    }

    protected AbstractSource(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    synchronized public void open(Messenger messages) throws IOException {
        if (opened)
            throw new IOException("already open");
        this.messages = messages;
        openSource();
        opened = true;
    }

    synchronized public void close() throws IOException {
        if (opened) {
            opened = false;
            closeSource();
        }
    }

    protected void failIfClosed() throws IOException {
        if (!opened)
            throw new IOException("closed");
    }

    public byte[] readPacket() throws IOException {
        failIfClosed();

        try {
            return check(readSourcePacket());
        }
        catch (IOException e) {
            close();
            throw e;
        }
    }

    synchronized public boolean writePacket(byte[] packet) throws IOException {
        failIfClosed();

        try {
            return writeSourcePacket(check(packet));
        }
        catch (IOException e) {
            close();
            throw e;
        }
    }

    protected byte[] check(byte[] packet) throws IOException {
        return packet;
    }

    // Implementation interfaces
    abstract protected void openSource() throws IOException;
    abstract protected void closeSource() throws IOException;
    abstract protected byte[] readSourcePacket() throws IOException;
    protected boolean writeSourcePacket(byte[] packet) throws IOException {
        // Default writer swallows packets
        return true;
    }
}
