package edu.iupui.MobileDeluge;

/**
 * Created by Omor on 15/09/2016.
 */
/**
 *
 * The listener interface receives incoming packets.
 *
 * @author  <A HREF="http://www.cs.berkeley.edu/~mikechen/">Mike Chen</A>
 * @since   1.1.6
 */

public interface PacketListenerIF extends java.util.EventListener {
    public void packetReceived(byte[] packet);
}
