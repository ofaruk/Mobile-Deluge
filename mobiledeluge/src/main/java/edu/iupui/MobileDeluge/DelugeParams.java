package edu.iupui.MobileDeluge;

/**
 * Created by Omor on 13/10/2016.
 */
public class DelugeParams {

    public static final int FM_AMID = 0x53;
    public static final int DM_AMID = 0x54;
    public static final int FM_COMMAND = 1;
    public static final int DM_COMMAND = 0;

    public static final int SERIAL_DATA_LENGTH = 28 - (1+1+4+2);

    public static final int FM_CMD_ERASE     = 0;
    public static final int FM_CMD_WRITE     = 1;
    public static final int FM_CMD_READ      = 2;
    public static final int FM_CMD_CRC       = 3;
    public static final int FM_CMD_ADDR      = 4;
    public static final int FM_CMD_SYNC      = 5;
    public static final int FM_CMD_IDENT     = 6;

    //Commands for DelugeManager
    public static final short DM_CMD_STOP = 1;
    public static final int DM_CMD_LOCAL_STOP = 2;
    public static final int DM_CMD_ONLY_DISSEMINATE = 3;
    public static final short DM_CMD_DISSEMINATE_AND_REPROGRAM = 4;
    public static final int DM_CMD_REPROGRAM = 5;
    public static final int DM_CMD_BOOT = 6;

    public static final int ERROR_SUCCESS = 0;
    public static final int ERROR_FAIL    = 1;

    //Deluge parameters
    public static final int DELUGE_MAX_PAGES    = 128;
    public static final int DELUGE_IDENT_OFFSET = 0;
    public static final int DELUGE_IDENT_SIZE   = 128;

}
