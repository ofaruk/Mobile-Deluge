package edu.iupui.MobileDeluge;

/**
 * Created by Omor on 4/09/2016.
 */
public interface MessageListener {
    /**
     * MessageListener interface (listen to tinyos messages).<p>
     *
     * An interface for listening to messages built from
     * net.tinyos.message.Message
     *
     * @version	1, 15 Jul 2002
     * @author	David Gay
     */

        /**
         * This method is called to signal message reception. The destination of
         * message m is to.
         * @param to the destination of the message (Note: to is only valid
         *   when using TOSBase base stations)
         * @param m the received message
         */
        public void messageReceived(int to, Message m);
    }

