package edu.iupui.MobileDeluge;

/**
 * Created by Omor on 4/09/2016.
 */

import java.io.*;

/**
 * Sender class (send tinyos messages).<p>
 *
 * A sender class provides a simple interface built on Message for
 * sending tinyos messages to a SerialForwarder
 *
 * @version	2, 24 Jul 2003
 * @author	David Gay
 */
public class Sender {
    // If true, dump packet contents that are sent
    private static final boolean VERBOSE = false;

    PhoenixSource sender;

    /**
     * Create a sender talking to PhoenixSource forwarder. The group id of
     * sent packets is not set.
     * @param forwarder PhoenixSource with which we wish to send packets
     */
    public Sender(PhoenixSource forwarder) {
        sender = forwarder;
    }

    /**
     * Send m to moteId via this Sender's SerialForwarder
     * @param moteId message destination
     * @param m message
     * @exception IOException thrown if message could not be sent
     */
    synchronized public void send(int moteId, Message m) throws IOException {
        int amType = m.amType();
        byte[] data = m.dataGet();

        if (amType < 0) {
            throw new IOException("unknown AM type for message " +
                    m.getClass().getName());
        }

        SerialPacket packet =
                new SerialPacket(SerialPacket.offset_data(0) + data.length);
        packet.set_header_dest(moteId);
        packet.set_header_type((int)amType);
        packet.set_header_length((int)data.length);
        packet.dataSet(data, 0, packet.offset_data(0), data.length);

        byte[] packetData = packet.dataGet();
        byte[] fullPacket = new byte[packetData.length + 1];
        fullPacket[0] = Serial.TOS_SERIAL_ACTIVE_MESSAGE_ID;
        System.arraycopy(packetData, 0, fullPacket, 1, packetData.length);
        sender.writePacket(fullPacket);
        if (VERBOSE) Dump.dump("sent", fullPacket);
    }
}