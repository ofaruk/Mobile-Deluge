package edu.iupui.MobileDeluge;

/**
 * Created by Omor on 17/10/2016.
 */

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;

import java.io.IOException;

import static xdroid.toaster.Toaster.toast;

/**
 * establish serial communication between the base station mote and the computer
 */

public class SerialComm implements MessageListener {
    Context context;
    private String nodeType = "UNKNOWN";
    private MoteIF moteIF;
    private boolean programable = false;
    private Handler mHandler;
    Bundle serialCommBundle = new Bundle();
    public static final int MSG_NODE_READY = 5; //pre was 2
    public static final int MSG_DETECT_NEIGHBOR = 6; // pre was 3
    public static final int MSG_DETECT_NEIGHBOR_SUBSET = 7;
    //command is sent to nodeID 0
    private short nodeID = 0;
    public SerialComm(MoteIF moteif,Context context, Handler uiHandler) {
        this.moteIF = moteif;
        this.context = context;
        this.mHandler = uiHandler;
        moteIF.registerListener(new SerialReplyMsg(), this);
    }
    public SerialComm(MoteIF moteif,Handler threadHandler) {
        this.moteIF = moteif;
        this.mHandler = threadHandler;
        moteIF.registerListener(new SerialReplyMsg(), this);
    }

    void getWriteThreadHandler() {
        if (mHandler == null)
        mHandler = WriteThread.getWriteThreadHandler();
    }

    private boolean isProgramable(float voltage) {
        return (voltage >= 2.7 ); // reference voltage is from http://tinyos.stanford.edu/tinyos-wiki/index.php/Deluge_T2
    }

    public void deRegisterListener() {
        moteIF.deregisterListener(new SerialReplyMsg(), this);
    }

    // send command packets
    public void SubSend(short cmd, short node_count, int[] node_list, short channel) {
        SerialCmdMsg payload = new SerialCmdMsg();
        try {
            try {
                Thread.sleep(200);
            }
            catch (InterruptedException exception) {
            }

			/*	static final int ABORT = 0;	// dissemination aborted, return to original status
				static final int DISS = 1;	// start dissemination
				static final int REPORT = 2;
				static final int REPORT_SUBSET = 3;
				static final int REBOOT = 4;
			 * */
            switch(cmd) {
                case Constants.ABORT:
                case Constants.DISS:
                    toast("\nSending command to: ");
                    for(int j = 0; j < node_count; j++) {
                        toast(node_list[j] + " ");
                    }
                    break;
                case Constants.REPORT:
                    //System.out.print("\nDetecting neighbors...");
                    break;
                case Constants.REPORT_SUBSET:
                    //System.out.print("\nDetecting targets: ");
                    for(int j = 0; j < node_count; j++) {
                        //System.out.print(node_list[j] + " ");
                    }
                    break;
                case Constants.REBOOT:
                    //System.out.print("\nReboot target: " + node_list[0]);

                    break;
                case Constants.SET_ADDRESS:
                    //System.out.print("\nSet the address of " + node_list[0] + " to " + node_list[1] + "...");
                    //System.out.print("\n\tPlease send a DETECT command later to check the new ID");

                    break;
                default:
                    break;

            }
            payload.set_cmd(cmd);
            payload.set_nodeCount(node_count);
            payload.set_nodeList(node_list);
            payload.set_newChannel(channel);
            moteIF.send(0, payload);
        }
        catch (IOException exception) {
            //System.err.println("Exception thrown when sending packets. Exiting.");
            toast("Exception thrown when sending packets. Exiting.");
            System.err.println(exception);
        }
    }

    public void messageReceived(int to, Message message) {
		System.out.println("msg received");
        //deRegisterListener();
        SerialReplyMsg msg = (SerialReplyMsg) message;
        float voltage = 0;
        switch(msg.get_nodeType()) {
            case Constants.IRIS_MOTE:
                nodeType = "IRIS";
                //	if(voltage != 0) {
                // if there is no sensorboard attached with the mote,
                // voltage reading will be 0.
                voltage = 1100*1024 / msg.get_nodeVoltage();
                programable = isProgramable(voltage);
                serialCommBundle.putFloat("voltage",voltage);
                serialCommBundle.putBoolean("programable",programable);
                //	}
                break;
            case Constants.MICAZ_MOTE:
                nodeType = "MICAz";
                //	if(voltage != 0) {
                voltage = 1223*1024 / msg.get_nodeVoltage();
                programable = isProgramable(voltage);
                serialCommBundle.putFloat("voltage",voltage);
                serialCommBundle.putBoolean("programable",programable);
                //	}
                break;
            case Constants.TELOSB_MOTE:
                nodeType = "TelosB";
                voltage = (float)msg.get_nodeVoltage() * 5 * 1000 / 4096;
                programable = isProgramable(voltage);
                System.out.println(programable);
                serialCommBundle.putFloat("voltage",voltage);
                serialCommBundle.putBoolean("programable",programable);
                System.out.println(serialCommBundle.getFloat("voltage"));
                break;
            case Constants.EPIC_MOTE:
                nodeType = "Epic";
                break;
            case Constants.MULLE_MOTE:
                nodeType = "Mulle";
                break;
            case Constants.TINYNODE_MOTE:
                nodeType = "TinyNode";
                break;
            case Constants.UNKNOWN:
                nodeType = "UNKNOWN";
                break;
        }

        if(msg.get_cmd() == Constants.NODE_READY) {
            getWriteThreadHandler();
            android.os.Message msgToThread = android.os.Message.obtain(mHandler, WriteThread.MSG_FROM_SERIALCOMM_NODEREADY,msg);
            msgToThread.setData(serialCommBundle);
            mHandler.sendMessage(msgToThread);

        }
        else if(msg.get_cmd() == Constants.NODE_ABORTED) {
            //System.out.println("Node " + msg.get_nodeid() + " is ABORTED!");
            getWriteThreadHandler();
            android.os.Message msgToThread = android.os.Message.obtain(mHandler, WriteThread.MSG_FROM_SERIALCOMM_NODEABORT,msg);
            mHandler.sendMessage(msgToThread);
        }
        else if(msg.get_cmd() == Constants.NODE_REPORT) {
                getWriteThreadHandler();
                android.os.Message msgToThread = android.os.Message.obtain(mHandler, WriteThread.MSG_FROM_SERIALCOMM_NODEREPORT,msg);
                msgToThread.setData(serialCommBundle);
                mHandler.sendMessage(msgToThread);

        }

        else if(msg.get_cmd() == Constants.NODE_REPORT_SUBSET) {

            getWriteThreadHandler();
            android.os.Message msgToThread = android.os.Message.obtain(mHandler, WriteThread.MSG_FROM_SERIALCOMM_NODEREPORT_SUBSET,msg);
            msgToThread.setData(serialCommBundle);
            mHandler.sendMessage(msgToThread);
        }

        else if(msg.get_cmd() == Constants.NODE_REBOOT) {
            //System.out.println("Node " + msg.get_nodeid() + " is REBOOTED!");

        }
        else if(msg.get_cmd() == Constants.NODE_NO_RESPONSE) {
            //System.out.println("Node " + msg.get_nodeid() + " NO RESPONSE!");
        }
    }

    /**
     * Stop the connection for new serial packets and Deluge commands
     * Always stop the phoenix before issuing next serial command/Deluge command
     * */

    /**
     * establish a serial connection, then send serial packets
     **/
    public void sendCmd(short cmd, short node_count, int[] node_list, short channel) {
        SubSend(cmd, node_count, node_list, channel);
    }
}
