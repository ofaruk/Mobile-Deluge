package edu.iupui.MobileDeluge;

/**
 * Created by Omor on 15/09/2016.
 */
public interface PhoenixError {
    /**
     * This method is called to signal a problem with a packet source
     * @param e The IOExcpetion that occured
     */
    public void error(java.io.IOException e);
}