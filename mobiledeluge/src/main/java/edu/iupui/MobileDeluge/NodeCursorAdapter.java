package edu.iupui.MobileDeluge;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import edu.iupui.MobileDeluge.Database.DelugeContract;

/**
 * Created by Omor on 21/07/2017.
 */

public class NodeCursorAdapter extends CursorAdapter {
    public NodeCursorAdapter(Context context, Cursor c) {
        super(context,c,0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item,parent,false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView nodeID = (TextView) view.findViewById(R.id.cat_id);
        TextView version = (TextView) view.findViewById(R.id.cat_version);
        TextView voltage = (TextView) view.findViewById(R.id.cat_voltage);
        TextView programable = (TextView) view.findViewById(R.id.cat_programable);


        int nodeColumnIndex = cursor.getColumnIndex(DelugeContract.NodeTable.COLUMN_NODE_ID);
        int verColumnIndex = cursor.getColumnIndex(DelugeContract.NodeTable.COLUMN_VERSION);
        int volColumnIndex = cursor.getColumnIndex(DelugeContract.NodeTable.COLUMN_VOLTAGE);
        int progColumnIndex = cursor.getColumnIndex(DelugeContract.NodeTable.COLUMN_PROGRAMABLE);

        nodeID.setText("Node id: " + String.valueOf(cursor.getInt(nodeColumnIndex)));
        version.setText("Version: " + String.valueOf(cursor.getFloat(verColumnIndex)));
        voltage.setText(("Voltage: " + String.valueOf(cursor.getFloat(volColumnIndex))));
        programable.setText("Programable: " + cursor.getString(progColumnIndex));
    }
}
