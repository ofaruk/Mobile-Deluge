package edu.iupui.MobileDeluge;

/**
 * Created by Omor on 4/09/2016.
 */
import java.io.*;

public interface ByteSource
{
    public void open();
    public void close();
    public byte readByte() throws IOException;
    public void writeBytes(byte[] bytes);
}
