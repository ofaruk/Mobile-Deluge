package edu.iupui.MobileDeluge;

/**
 * Created by Omor on 4/09/2016.
 */
public interface TimestampedPacketSource extends PacketSource {
    /**
     * Returns timestamp in milliseconds for last received packet
     * @return
     */
    public long getLastTimestamp();

    /**
     * Whether current negotiated session supports timestamping (can be negotiated base version)
     * @return
     */
    public boolean supportsTimestamping();
}