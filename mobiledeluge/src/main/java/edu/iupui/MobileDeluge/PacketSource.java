package edu.iupui.MobileDeluge;

/**
 * Created by Omor on 4/09/2016.
 */
import java.io.*;

public interface PacketSource
{
    /**
     * Get PacketSource name
     * @return the name of this packet source, valid for use with
     * <code>BuildSource.makeSource</code>.
     */
    public String getName();

    /**
     * Open a packet source
     * @param messages A destination for informative messages from the
     *   packet source, or null to discard these.
     * @exception IOException If the source could not be opened
     */
    public void open(Messenger messages) throws IOException;

    /**
     * Close a packet source. Closing a source must force any
     * running <code>readPacket</code> and <code>writePacket</code>
     * operations to terminate with an IOException
     * @exception IOException Thrown if a problem occured during closing.
     *   The source is considered closed even if thos occurs.
     *   Closing a closed source does not cause this exception
     */
    public void close() throws IOException;

    /**
     * Read a packet
     * @return The packet read (newly allocated). The format is described
     *   above
     * @exception IOException If the source detected a problem. The source
     *   is automatically closed.
     */
    public byte[] readPacket() throws IOException;

    /**
     * Write a packet
     * @param packet The packet to write. The format is decribed above.
     * @return Some packet sources will return false if the packet
     *   could not be written.
     */
    public boolean writePacket(byte[] packet) throws IOException;
}