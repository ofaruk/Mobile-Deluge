package edu.iupui.MobileDeluge.Activity;

import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

import edu.iupui.MobileDeluge.Database.DelugeContract;
import edu.iupui.MobileDeluge.NodeCursorAdapter;
import edu.iupui.MobileDeluge.R;

/**
 * Created by Omor on 21/07/2017.
 */

public class DatabaseViewActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private NodeCursorAdapter mCursorAdapter;
    private static final int LOADERID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.database_view);
        mCursorAdapter = new NodeCursorAdapter(this,null);
        ListView nodeListView = (ListView) findViewById(R.id.list_view_node);
        View emptyView = findViewById(R.id.empty_view);
        nodeListView.setEmptyView(emptyView);
        getLoaderManager().initLoader(LOADERID,null,this);
        nodeListView.setAdapter(mCursorAdapter);

    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String[] projection = new String[]{DelugeContract.NodeTable._ID,DelugeContract.NodeTable.COLUMN_NODE_ID,DelugeContract.NodeTable.COLUMN_VERSION,
                                           DelugeContract.NodeTable.COLUMN_VOLTAGE,DelugeContract.NodeTable.COLUMN_PROGRAMABLE
                                           };
        return new CursorLoader(this,DelugeContract.NodeTable.CONTENT_URI,projection,null,null,null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mCursorAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mCursorAdapter.swapCursor(null);
    }
}
