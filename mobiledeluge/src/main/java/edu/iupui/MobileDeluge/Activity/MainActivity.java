package edu.iupui.MobileDeluge.Activity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.ColorStateList;
import android.database.ContentObserver;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import edu.iupui.MobileDeluge.Constants;
import edu.iupui.MobileDeluge.Database.DelugeContract;
import edu.iupui.MobileDeluge.DelugeParams;
import edu.iupui.MobileDeluge.Logs;
import edu.iupui.MobileDeluge.MoteIF;
import edu.iupui.MobileDeluge.Nodes;
import edu.iupui.MobileDeluge.R;
import edu.iupui.MobileDeluge.SerialComm;
import edu.iupui.MobileDeluge.SerialReplyMsg;
import edu.iupui.MobileDeluge.UsbService;
import edu.iupui.MobileDeluge.WriteThread;

import static xdroid.toaster.Toaster.toast;

public class MainActivity extends AppCompatActivity {

    /*
     * Notifications from UsbService will be received here.
     */
    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case UsbService.ACTION_USB_PERMISSION_GRANTED: // USB PERMISSION GRANTED
                    Toast.makeText(context, "USB Ready", Toast.LENGTH_SHORT).show();
                    //usbService.startConnectionThread(baudRate);
                    moteIF = usbService.getMoteIF();
                    //serialComm = getSerialComm();

                    break;
                case UsbService.ACTION_USB_PERMISSION_NOT_GRANTED: // USB PERMISSION NOT GRANTED
                    Toast.makeText(context, "USB Permission not granted", Toast.LENGTH_SHORT).show();
                    break;
                case UsbService.ACTION_NO_USB: // NO USB CONNECTED
                    Toast.makeText(context, "No USB connected", Toast.LENGTH_SHORT).show();
                    break;
                case UsbService.ACTION_USB_DISCONNECTED: // USB DISCONNECTED
                    Toast.makeText(context, "USB disconnected", Toast.LENGTH_SHORT).show();
                    moteIF = null;
                    serialComm = null;
                    if (writeThread != null) {
                        writeThread.quit();
                    }
                    writeThread = null;
                    break;
                case UsbService.ACTION_USB_NOT_SUPPORTED: // USB NOT SUPPORTED
                    Toast.makeText(context, "USB device not supported", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
    private UsbService usbService;
    private TextView output;
    private EditText editText;
    private MyHandler mHandler;
    private MoteIF moteIF;
    private boolean mBound;
    private boolean busy = false;
    private SerialComm serialComm;
    private WriteThread writeThread;
    //deluge UI button and Text fields
    private EditText inputNodes;
    private EditText inputSubsetNodes;
    private FloatingActionButton fab;

    //adding deluge variables here

    private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String time = df.format(new Date());


    private int[] targetList = new int[Constants.MAX_NUM_IDS];
    private short targetCount = 0;
    private String[] nodeList;

    private List<Nodes> recordList = new ArrayList<Nodes>();
    private int recordCount = 0;

    boolean newCycle = false;
    private short nodeCmd = 0xFF;
    private short newChannel = 20;
    private float versionNumber;
    private int targetImageNumber = 1; //default is telosB
    private final static int TELOS_BAUD_RATE = 115200;
    private final static int MICAZ_BAUD_RATE = 57600;
    public static int baudRate = TELOS_BAUD_RATE;

    private String log_string;

    //Message Constants that is received in the Handle Message method

    public static final int DISS_COMMAND_SENT = 1;

    //helper method to clear the screen

    private void clearScreen() {
        output.setText("");
    }

    //helper method to resolve int to nodetype string

    private String resolveNode(int nodeCode) {
        if (nodeCode == Constants.TELOSB_MOTE)
            return "TelosB";
        else if (nodeCode == Constants.MICAZ_MOTE)
            return "MicaZ";
        else if (nodeCode == Constants.IRIS_MOTE)
            return "IRIS";
        else
            return "Unknown";
    }

    /**
     * Intialize the write thread and start it
     */
    public void initializeWriteThread() {
        if (writeThread == null) {
            writeThread = new WriteThread(mHandler);
            writeThread.start();
            writeThread.initializeHandler();

        }
    }


    private final ServiceConnection usbConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName arg0, IBinder arg1) {
            usbService = ((UsbService.UsbBinder) arg1).getService();
            usbService.setHandler(mHandler);
            Toast.makeText(getBaseContext(), "Service is Connected", Toast.LENGTH_SHORT).show();
            mBound = true;
            //moteIF = null;

        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            Toast.makeText(getBaseContext(), "Service is disconnecting", Toast.LENGTH_SHORT).show();
            mBound = false;
            usbService = null;
            serialComm = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mHandler = new MyHandler(this);
        output = (TextView) findViewById(R.id.output);
        //inputNodes = (EditText) findViewById(R.id.nodeList);
        //output.setMovementMethod(new ScrollingMovementMethod());
        //inputSubsetNodes = (EditText) findViewById(R.id.detect_subset_input);
        Button connectButton = (Button) findViewById(R.id.dm);
        connectButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
               /* String nodes = inputNodes.getText().toString().trim();
                String[] nodeList = (nodes.split(" "));
                //System.out.println(nodeList.length);
                clearScreen();
                if (nodeList.length > 0) {
                    connect(nodeList, true);
                }*/
                showLongClickDialog();
                return true;
            }
        });
        Toolbar myToolbar = (Toolbar) findViewById(R.id.deluge_toolbar);
        setSupportActionBar(myToolbar);
        updateTitleBarTarget(targetImageNumber);
        updateTitleBarVersion();
        updateTitleBarChannel();

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, DatabaseViewActivity.class);
                startActivity(intent);
            }
        });

        getContentResolver().registerContentObserver(DelugeContract.NodeTable.CONTENT_URI, true, new ContentObserver(null) {
            @Override
            public void onChange(boolean selfChange) {
                super.onChange(selfChange);
                changeFabColor();
            }
        });
        //System.out.println("Oncreate is called by " + Thread.currentThread().getId());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        setFilters();  // Start listening notifications from UsbService
        System.out.println("OnResume is called by " + Thread.currentThread().getId());
        startService(UsbService.class, usbConnection, null); // Start UsbService(if it was not started before) and Bind it
        getMoteIF();
    }

    @Override
    public void onPause() {
        unregisterReceiver(mUsbReceiver);
        super.onPause();
    }

    @Override
    public void onStop() {
        mBound = false;
        super.onStop();
    }

    @Override
    public void onDestroy() {
        unbindService(usbConnection);
        if (writeThread != null) {
            writeThread.quit();
        }
        super.onDestroy();
    }

    private void changeFabColor() {
        fab.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
    }

    /**
     * Helper method to insert some dummy data in the database
     *
     * @param
     * @return
     */
    public void insertDummyData() {
        ContentValues values = new ContentValues();
        values.put(DelugeContract.NodeTable.COLUMN_NODE_ID, 20);
        values.put(DelugeContract.NodeTable.COLUMN_PROGRAMABLE, "Yes");
        values.put(DelugeContract.NodeTable.COLUMN_VERSION, 2.56);
        values.put(DelugeContract.NodeTable.COLUMN_VOLTAGE, 2.90);
        Uri result = getContentResolver().insert(DelugeContract.NodeTable.CONTENT_URI, values);
    }

    private void deleteAllNodes() {
        int rowsDeleted = getContentResolver().delete(DelugeContract.NodeTable.CONTENT_URI, null, null);
        toast(rowsDeleted + " rows deleted from the node database");
    }

    //invocations after menu item selection callback received in here
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.channel_settings:
                showChangeChannelDialog();
                insertDummyData();
                return true;
            case R.id.target_settings:
                showChangeTargetDialog();
                deleteAllNodes();
                return true;
            case R.id.base_settings:
                showChangeBaseDialog();
                return true;
            case R.id.version_settings:
                showChangeVersionDialog();
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void updateTitleBarChannel() {
        TextView channel_toolbar = (TextView) findViewById(R.id.toolbar_text_channel);
        channel_toolbar.setText("Ch " + String.valueOf(newChannel));
    }

    private void updateTitleBarTarget(int i) {
        TextView target_text_toolbar = (TextView) findViewById(R.id.toolbar_text_target);
        if (i == 1) {
            target_text_toolbar.setText(" TelosB");
        } else if (i == 2) {
            target_text_toolbar.setText(" MicaZ");
        } else {
            target_text_toolbar.setText(" Iris");
        }
    }

    private void updateTitleBarVersion() {
        TextView version_toolbar = (TextView) findViewById(R.id.toolbar_text_version);
        version_toolbar.setText("V" + String.valueOf(versionNumber) + " ");
    }

    //popup dialog implementation for changing the channel

    public void showChangeChannelDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.channel_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.channel_input);

        dialogBuilder.setTitle("Change Channel");
        dialogBuilder.setMessage("Enter new channel below");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                String input_channel_string = edt.getText().toString();
                if (input_channel_string.equals("")) return;
                System.out.println(input_channel_string);
                Short input_channel = Short.parseShort(edt.getText().toString());
                if (input_channel > 1 && input_channel < 100) {
                    newChannel = (input_channel);
                    updateTitleBarChannel();
                    toast("channel set to " + newChannel);
                } else
                    toast("Invalid channel number");
                toast("Operating on Channel " + newChannel);
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                toast("Operating on Channel " + newChannel);
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }


    public void showChangeVersionDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.version_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.version_input);

        dialogBuilder.setTitle("Change Version");
        dialogBuilder.setMessage("Enter new Version below");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                String input_version_string = edt.getText().toString();
                if (input_version_string.equals("")) return;
                Float version = Float.parseFloat(edt.getText().toString());
                if (version > 0) {
                    versionNumber = version;
                    updateTitleBarVersion();
                    toast("version set to " + versionNumber);
                } else
                    toast("Invalid version number");
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                toast("Current version set to " + versionNumber);
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void showLongClickDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.nodeinput_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.nodeid_input);

        dialogBuilder.setTitle("Input node ID");
        dialogBuilder.setMessage("Enter ids seperated by space");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                String input_nodes = edt.getText().toString().trim();
                if (input_nodes.equals("")) return;
                nodeList = (input_nodes.split(" "));
                if (nodeList.length > 0) {
                    connect(nodeList, true);

                }

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                toast("cancelled");
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void showInputNodesDialog() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.nodeinput_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.nodeid_input);

        dialogBuilder.setTitle("Input node ID");
        dialogBuilder.setMessage("Enter ids seperated by space");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                String input_nodes = edt.getText().toString().trim();
                if (input_nodes.equals("")) return;
                nodeList = (input_nodes.split(" "));
                if (nodeList.length > 0) {
                    try {
                        detect();
                    } catch (IOException e) {
                        toast("Exception while running the detect command");
                    }
                    connect(nodeList, false);

                }

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                toast("cancelled");
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void showCommandFiveDialog() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.nodeinput_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.nodeid_input);

        dialogBuilder.setTitle("Input node ID");
        dialogBuilder.setMessage("Enter ids seperated by space");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                String input_nodes = edt.getText().toString().trim();
                if (input_nodes.equals("")) return;
                nodeList = (input_nodes.split(" "));
                if (nodeList.length > 0) {
                    try {
                        abort(nodeList);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                toast("cancelled");
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }


    public void showCommandNineDialog() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.nodeinput_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.nodeid_input);

        dialogBuilder.setTitle("Input node ID");
        dialogBuilder.setMessage("Enter ids seperated by space");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                String input_nodes = edt.getText().toString().trim();
                if (input_nodes.equals("")) return;
                nodeList = (input_nodes.split(" "));
                if (nodeList.length > 0) {
                    detect_subset(nodeList);
                }
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                toast("cancelled");
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void showChangeTargetDialog() {


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.target_spinner, null);
        dialogBuilder.setView(dialogView);

        final Spinner spinner = (Spinner) dialogView.findViewById(R.id.target_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.target_list, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (l == 0) {
                    targetImageNumber = 1; //telosb is selected assuming first copied image in the mote is for TelosB
                } else if (l == 1) {
                    targetImageNumber = 2; //micaz is selected
                } else
                    targetImageNumber = 3; //Iris is selected

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //do nothing
            }
        });
        dialogBuilder.setTitle("Change Target");
        dialogBuilder.setPositiveButton("Select", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                updateTitleBarTarget(targetImageNumber);
                toast("Target Image Number is " + targetImageNumber);

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                toast("Target Image Number is " + targetImageNumber);
            }
        });

        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void showChangeBaseDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.base_spinner, null);
        dialogBuilder.setView(dialogView);

        final Spinner spinner = (Spinner) dialogView.findViewById(R.id.base_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.base_mote_list, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (l == 0) {
                    baudRate = TELOS_BAUD_RATE; //telosb is selected assuming first copied image in the mote is for TelosB
                } else if (l == 1) {
                    baudRate = MICAZ_BAUD_RATE; //micaz is selected
                } else
                    baudRate = MICAZ_BAUD_RATE; //Iris is selected

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //do nothing
            }
        });
        dialogBuilder.setTitle("Change Target");
        dialogBuilder.setPositiveButton("Select", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                toast("Baud rate is set to  " + baudRate);

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                toast("Baud rate is set to " + baudRate);
            }
        });

        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    private void startService(Class<?> service, ServiceConnection serviceConnection, Bundle extras) {
        if (!UsbService.SERVICE_CONNECTED) {
            Intent startService = new Intent(this, service);
            if (extras != null && !extras.isEmpty()) {
                Set<String> keys = extras.keySet();
                for (String key : keys) {
                    String extra = extras.getString(key);
                    startService.putExtra(key, extra);
                }
            }
            //do not start the service as we dont want it to run it indefinitely in the system
            //startService(startService);
        }
        Intent bindingIntent = new Intent(this, service);
        bindService(bindingIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    private void setFilters() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbService.ACTION_USB_PERMISSION_GRANTED);
        filter.addAction(UsbService.ACTION_NO_USB);
        filter.addAction(UsbService.ACTION_USB_DISCONNECTED);
        filter.addAction(UsbService.ACTION_USB_NOT_SUPPORTED);
        filter.addAction(UsbService.ACTION_USB_PERMISSION_NOT_GRANTED);
        registerReceiver(mUsbReceiver, filter);
    }

    private void printOnUI(String text) {
        TextView output = (TextView) findViewById(R.id.output);
        if (output != null) {
            output.append(text + "\n");
        }
    }


    void getMoteIF() {
        try {
            moteIF = usbService.getMoteIF();
        } catch (NullPointerException e) {
            toast("MoteIF returned NULL");
        }
    }



    /*
     * This handler will be passed to UsbService. Data received from serial port is displayed through this handler
     */


    private static class MyHandler extends Handler {
        private final WeakReference<MainActivity> mActivity;

        public MyHandler(MainActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UsbService.MESSAGE_FROM_SERIAL_PORT:
                    //currently not used
                    break;

                case SerialComm.MSG_NODE_READY:
                    SerialReplyMsg replyMsg = (SerialReplyMsg) msg.obj;
                    mActivity.get().printOnUI(("Node " + replyMsg.get_nodeid() + " is READY" + " Type: " + mActivity.get().resolveNode(replyMsg.get_nodeType()) + " Version: "
                            + replyMsg.get_appVersion() + " Voltage (mV): " + msg.getData().getFloat("voltage") + "\n"));
                    break;

                case SerialComm.MSG_DETECT_NEIGHBOR:
                    SerialReplyMsg detectMsg = (SerialReplyMsg) msg.obj;
                    mActivity.get().printOnUI(("Node " + detectMsg.get_nodeid() + " is READY" + " Type: " + mActivity.get().resolveNode(detectMsg.get_nodeType()) + " Version: "
                            + detectMsg.get_appVersion() + " Voltage (mV): " + msg.getData().getFloat("voltage") + " Programable = " + msg.getData().getBoolean("programable") + "\n"));
                    break;

                case SerialComm.MSG_DETECT_NEIGHBOR_SUBSET:
                    SerialReplyMsg detectSubsetMsg = (SerialReplyMsg) msg.obj;
                    mActivity.get().printOnUI(("Node " + detectSubsetMsg.get_nodeid() + " is READY" + " Type: " + mActivity.get().resolveNode(detectSubsetMsg.get_nodeType()) + " Version: "
                            + detectSubsetMsg.get_appVersion() + " Voltage (mV): " + msg.getData().getFloat("voltage") + " Programable = " + msg.getData().getBoolean("programable") + "\n"));
                    break;


                case DISS_COMMAND_SENT:
                    if (msg.arg1 == 1)
                        toast("Command Sent");
                    else
                        toast("Commnad sent but not acked by mote");
                    break;
                case Constants.NODE_ABORTED:
                    SerialReplyMsg rcvdMsg = (SerialReplyMsg) msg.obj;
                    toast("Node " + rcvdMsg.get_nodeid() + " is ABORTED!");
                    break;
                default:
                    toast("Uknown Message Received in UI handle Msg");
            }
        }
    }
    //specific button implementation for the commands

    //Option 2 will be run with the number of nodes that will be given as input in the User Interface

    public void commandTwo(View view) {
        //inputNodes = (EditText) findViewById(R.id.nodeList);
        //String nodes = inputNodes.getText().toString().trim();
        //String[] nodeList = (nodes.split(" "));
        //System.out.println(nodeList.length);
        showInputNodesDialog();
        clearScreen();

    }


    //importing Deluge commnads from here

    /*
     * Option 2, connect target nodes
     * */
    public void connect(String[] ids_list, boolean forced) {
        // always send a "-s" command to Deluge Base when trying to connect new targets.
        // In case of connecting wrong targets while deluge base is sending image
        if (!busy && moteIF != null) {
            busy = true;
            Deluge_s();

            int i;
            boolean id_error = false;
            for (i = 0; i < targetList.length; i++) {
                targetList[i] = 0;
            }
            nodeCmd = Constants.DISS;

            for (i = 0; i < ids_list.length; i++) {
                try {
                    targetList[i] = Integer.decode(ids_list[i]);
                } catch (NumberFormatException e) {
                    id_error = true;
                    break;
                }
            }

            if (id_error) {
                // if there some id is not valid
                toast("Invalid Node ID!");
                return;
            }

            targetCount = (short) ids_list.length;
            //serialComm.sendCmd(nodeCmd, targetCount, targetList, newChannel);
            initializeWriteThread();
            if (forced) {
                toast("Forcing Connect Command");
                writeThread.sendMobileDelugeConnectCommand(moteIF, nodeCmd, targetCount, targetList, newChannel);
            } else {
                toast("Filtered Command Sent");
                try {
                    writeThread.sendVoltageFilteredConnectCommand(moteIF, nodeCmd, targetCount, targetList, newChannel);
                }
                catch (InterruptedException e) {
                    toast("Interrupted Exception Filtered Command");
                }
            }
            time = df.format(new Date());

        } else {
            Toast.makeText(getBaseContext(), "Unable to get moteIF instance", Toast.LENGTH_SHORT).show();
        }
        busy = false;
    }

    /*
	 * Option 3: deluge command: -dr
	 * */

    public void Deluge_dr(View view) throws IOException {
        if (!busy && moteIF != null) {
            busy = true;
            //TestDeluge testDeluge = new TestDeluge(moteIF,DelugeParams.DM_COMMAND,targetImageNumber);
            //testDeluge.dissAndReprogram();
            initializeWriteThread();
            //build the node list
            detect();
            writeThread.delugeDisseminationAndReboot(moteIF, DelugeParams.DM_COMMAND, targetImageNumber);
        }
        newCycle = true;

        busy = false;
    }

    /*
	 * Option 4: Deluge command: -s
	 * */
    public void callDeluge_s(View view) {
        Deluge_s();
    }

    public void Deluge_s() {
        if (!busy && moteIF != null) {
            busy = true;
            initializeWriteThread();
            writeThread.delugeStopDissemination(moteIF);
        }
        busy = false;
    }

    /*
	 * Option 5, abort
	 * */

    public void commandFive(View view) {
        /*inputNodes = (EditText) findViewById(R.id.abortList);
        String nodes = inputNodes.getText().toString().trim();
        String[] nodeList = (nodes.split(" "));*/
        showCommandFiveDialog();

    }

    public void abort(String[] ids_list) throws IOException {
        if (!busy && moteIF != null) {
            busy = true;
            int i;
            boolean id_error = false;

            targetCount = (short) ids_list.length;

            for (i = 0; i < targetList.length; i++) {
                targetList[i] = 0;
            }
            for (i = 0; i < ids_list.length; i++) {
                try {
                    targetList[i] = Integer.decode(ids_list[i]);
                } catch (NumberFormatException e) {
                    id_error = true;
                    continue;
                }
            }
            if (id_error) {
                // if there some id is not valid
                toast("\nInvalid Node ID!");
                return;
            }


            nodeCmd = Constants.ABORT;
            //if (ids_list[0].equals("a")) {
            // no need to change the node list
            //  toast("All nodes will be aborted!");
            //serialComm.sendCmd(nodeCmd, (short) targetCount, targetList, newChannel);
            initializeWriteThread();
            writeThread.sendMobileDelugeAbortCommand(moteIF, nodeCmd, targetCount, targetList, newChannel);

        }
        busy = false;
    }


    /*
	 * Option 8, detect neighbors
	 * */
    public void commandEight(View view) {
        clearScreen();
        try {
            detect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void detect() throws IOException {
        if (!busy && moteIF != null) {
            busy = true;
            nodeCmd = Constants.REPORT;
            int i;
            toast("Detecting neighbors... ");

            targetCount = 0;
            for (i = 0; i < targetList.length; i++) {
                // clear the target list
                targetList[i] = 0xFFFF;
            }
            //serialComm.sendCmd(nodeCmd, targetCount, targetList, newChannel);
            initializeWriteThread();
            writeThread.sendMobileDelugeDetectCommand(moteIF, nodeCmd, targetCount, targetList, newChannel);

        }
        busy = false;
    }

    /*
    * Option 8, detect neighbors
    * */
    public void commandNine(View view) {
        clearScreen();
        /*String nodes = inputSubsetNodes.getText().toString().trim();
        String[] nodeList = (nodes.split(" "));*/
        showCommandNineDialog();

    }

    public void detect_subset(String[] ids_list) {
        int i;
        boolean id_error = false;
        targetCount = 0;

        for (i = 0; i < targetList.length; i++) {
            targetList[i] = 0;
        }
        nodeCmd = Constants.REPORT_SUBSET;

        for (i = 0; i < ids_list.length; i++) {
            try {
                targetList[i] = Integer.decode(ids_list[i]);

            } catch (NumberFormatException e) {
                id_error = true;
                break;
            }
        }

        if (id_error) {
            // if there some id is not valid
            toast("Invalid Node ID!");
            return;
        }

        targetCount = (short) ids_list.length;

        initializeWriteThread();
        writeThread.sendMobileDelugeDetectSubsetCommand(moteIF, nodeCmd, targetCount, targetList, newChannel);
    }

}
