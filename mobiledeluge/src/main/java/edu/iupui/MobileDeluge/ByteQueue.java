package edu.iupui.MobileDeluge;

/**
 * Created by Omor on 17/09/2016.
 */
public class ByteQueue
{
    byte buffer[];
    int nbegin;
    int nend;

    int num_free_back()
    {
        return buffer.length - nend;
    }

    void left_justify_into( byte dest[] )
    {
        for( int i=nbegin,j=0; i<nend; i++,j++ )
            dest[j] = buffer[i];
        nend -= nbegin;
        nbegin = 0;
        buffer = dest;
    }

    synchronized void ensure_free( int len )
    {
        if( (nbegin + num_free_back()) < len )
        {
            int newlen = buffer.length * 2;
            int total = available() + len;
            while( newlen < total )
                newlen *= 2;
            left_justify_into( new byte[newlen] );
        }
        else if( num_free_back() < len )
        {
            left_justify_into( buffer );
        }
    }

    public int available()
    {
        return nend - nbegin;
    }

    public void push_back( byte b )
    {
        ensure_free(1);
        buffer[nend++] = b;
    }

    public void push_back( byte b[] )
    {
        push_back( b, 0, b.length );
    }

    public void push_back( byte b[], int off, int len )
    {
        ensure_free( len );
        int bend = off + len;
        while( off < bend )
            buffer[nend++] = b[off++];
    }

    public int pop_front()
    {
        if( available() > 0 )
            return ((int)buffer[nbegin++]) & 255;
        return -1;
    }

    public int pop_front( byte b[] )
    {
        return pop_front( b, 0, b.length );
    }

    public int pop_front( byte b[], int off, int len )
    {
        int n = available();
        if( n > len )
            n = len;
        int bend = off + len;
        while( off < bend )
            b[off++] = buffer[nbegin++];
        return n;
    }

    public ByteQueue()
    {
        this(64);
    }

    public ByteQueue( int initial_buffer_length )
    {
        buffer = new byte[ initial_buffer_length ];
        nbegin = 0;
        nend = 0;
    }
}

