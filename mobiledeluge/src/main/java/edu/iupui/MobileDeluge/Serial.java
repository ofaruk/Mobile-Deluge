package edu.iupui.MobileDeluge;

/**
 * Created by Omor on 4/09/2016.
 */
public class Serial  {
    public static final byte HDLC_CTLESC_BYTE = 125;
    public static final byte SERIAL_PROTO_ACK = 67;
    public static final byte TOS_SERIAL_802_15_4_ID = 2;
    public static final int SERIAL_PROTO_PACKET_UNKNOWN = 255;
    public static final byte SERIAL_PROTO_PACKET_NOACK = 69;
    public static final byte TOS_SERIAL_CC1000_ID = 1;
    public static final byte HDLC_FLAG_BYTE = 126;
    public static final byte TOS_SERIAL_ACTIVE_MESSAGE_ID = 0;
    public static final byte SERIAL_PROTO_PACKET_ACK = 68;
    public static final int TOS_SERIAL_UNKNOWN_ID = 255;
}