package edu.iupui.MobileDeluge;

/**
 * Created by Omor on 17/10/2016.
 */

public class Constants {
    // all the final variables must match those in Reprogrammer.h
    public static final int ABORT = 0;	// dissemination aborted, return to original status
    public static final int DISS = 1;	// start dissemination
    public static final int REPORT = 2;
    public static final int REPORT_SUBSET = 3;
    public static final int REBOOT = 4;
    public static final int SET_ADDRESS = 5;

    public static final int INIT_MSG = 255;


    // base to PC
    public static final int NODE_READY = 51;	// mote is ready
    public static final int NODE_ABORTED = 52;// mote returned to original status
    public static final int NODE_REPORT = 53;
    public static final int NODE_REPORT_SUBSET = 54;
    public static final int NODE_REBOOT = 55;
    public static final int NODE_SET_ADDRESS = 56;
    public static final int NODE_NO_RESPONSE = 0xFF;

    // max number of target node ids
   public  static final int MAX_NUM_IDS = 15;
    // definition of platforms

    public static final int IRIS_MOTE = 0xA1;
    public static final int MICAZ_MOTE = 0xA2;
    public static final int TELOSB_MOTE = 0xA3;
    public static final int EPIC_MOTE = 0xA4;
    public static final int MULLE_MOTE = 0xA5;
    public static final int TINYNODE_MOTE = 0xA6;
    public static final int UNKNOWN = 0xFF;

    public static final int ID = 0;
    public static final int TYPE = 1;
    public static final int FLAG = 2;
    public static final int TIME = 3;
}

