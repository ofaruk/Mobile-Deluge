package edu.iupui.MobileDeluge;

/**
 * Created by Omor on 17/10/2016.
 */

import android.os.Environment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/*
 * This class puts everything in to a log file
 * */

public class Logs {
    public static boolean does_log_exist(){
        File f = new File(Environment.getExternalStorageDirectory() + "data/logs.txt");
        if(f.exists()){
            return true;
        } else {
            return false;
        }
    }



    public static void log(String s) throws IOException {
        if(does_log_exist()){
            // log already existed, append the String into the log

            FileWriter outf = new FileWriter(Environment.getExternalStorageDirectory() + "data/logs.txt", true);
            outf.write(s);
            outf.close();

        } else{
            // create log file and put the String into the log
            // create the file
            // create the directory first if necessary
            //System.out.println("Log file does not exist, create log file...");
            //get the path to the external storage
            File logPath = (Environment.getExternalStorageDirectory());
            //System.out.println(logPath.toString());
            logPath = new File(logPath + "/data");
            if(!logPath.exists()) {
                if(!logPath.mkdir()) {
                    System.out.println("Directory creation failed");
                }
            }
            /*File dir = new File("data");
            if(!dir.exists()){
                if(!dir.mkdir()) {
                    System.out.println("\nCreate 'data' directory failed!");
                } else {
                    System.out.println("\nSuccessfully created 'data' directory");
                }
            }*/
            FileWriter outf = new FileWriter(logPath + "/logs.txt");
            BufferedWriter bw = new BufferedWriter(outf);
            bw.write(s);
            bw.close();
            System.out.println("Log file created successfully!");
        }
    }

    public static void main(String[] args) throws IOException {
        log("The first log\n");
        log("\nThe second log\n");
        log("\nThe third log\n");
    }
}

