package edu.iupui.MobileDeluge;

/**
 * Created by ofaruk on 9/29/16.
 */
import java.io.IOException;
public class TestSerial implements MessageListener {

    private MoteIF moteIF;

    public TestSerial(MoteIF moteIF) {
        this.moteIF = moteIF;
        this.moteIF.registerListener(new TestSerialMsg(), this);
    }

    public void sendPackets() {
        int counter = 0;
        TestSerialMsg payload = new TestSerialMsg();

        try {
            while (true) {
                System.out.println("Sending packet " + counter);
                payload.set_counter(counter);
                moteIF.send(0, payload);
                counter++;
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException exception) {
                }
            }
        } catch (IOException exception) {
            System.err.println("Exception thrown when sending packets. Exiting.");
            System.err.println(exception);
        }
    }

    public void messageReceived(int to, Message message) {
        TestSerialMsg msg = (TestSerialMsg) message;
        System.out.println("Received packet sequence number " + msg.get_counter());
    }

    private static void usage() {
        System.err.println("usage: TestSerial [-comm <source>]");
    }
}