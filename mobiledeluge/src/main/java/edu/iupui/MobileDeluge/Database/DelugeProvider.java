package edu.iupui.MobileDeluge.Database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by Omor on 17/07/2017.
 */

public class DelugeProvider extends ContentProvider {
    public final String LOG_TAG = DelugeProvider.class.getSimpleName();
    public static final int UPDATE_ACTION = 1;
    public static final int INSERT_ACTION = 0;
    //URI matcher code for the NODE_INFO table
    private static final int NODEINFO = 100;
    //URI matcher code for individual row of the nodeinfo table
    private static final int NODEINFO_ID = 101;
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sUriMatcher.addURI(DelugeContract.CONTENT_AUTHORITY,DelugeContract.PATH_NODEINFO,NODEINFO);
        sUriMatcher.addURI(DelugeContract.CONTENT_AUTHORITY,DelugeContract.PATH_NODEINFO + "/#",NODEINFO_ID);
    }
    private DelugeDbHelper mDbHelper;

    @Override
    public boolean onCreate() {
        mDbHelper = new DelugeDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        //get a readable database
        SQLiteDatabase database = mDbHelper.getReadableDatabase();
        Cursor cursor;
        int match = sUriMatcher.match(uri);
        switch(match) {
            case NODEINFO:
                cursor = database.query(DelugeContract.NodeTable.TABLE_NAME,projection,selection,selectionArgs,null,null,sortOrder);
                break;

            case NODEINFO_ID:
                selection = DelugeContract.NodeTable._ID + "=?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};
                cursor = database.query(DelugeContract.NodeTable.TABLE_NAME,projection,selection,selectionArgs,null,null,sortOrder);
                break;

            default:
                throw new IllegalArgumentException("Can not query Unknown Uri " + uri);
        }

        return cursor;
    }

    /**
     * Returns the MIME type of the Content URI
     * @param uri
     * @return MIME type of Content Uri
     */
    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        int match = sUriMatcher.match(uri);
        switch (match) {
            case NODEINFO:
                return DelugeContract.CONTENT_LIST_TYPE;
            case NODEINFO_ID:
                return DelugeContract.CONTENT_LIST_ITEM;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri + " with match " + match);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        int match = sUriMatcher.match(uri);
        switch (match) {
            case NODEINFO:
                return insertNode(uri,contentValues);
            default:
                throw new IllegalArgumentException("Insertion is not supported for this " + uri);
        }
    }

    private Uri insertNode(Uri uri, ContentValues contentValues) {
        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        long id = database.insert(DelugeContract.NodeTable.TABLE_NAME,null,contentValues);
        if (id == -1) {
            Log.e(LOG_TAG,"Failed to insert row for " + uri);
            return null;
        }
        else {
            getContext().getContentResolver().notifyChange(uri,null);
            return ContentUris.withAppendedId(uri,id);
        }
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int deleCount = 0;
        switch (match) {
            case NODEINFO:
                return database.delete(DelugeContract.NodeTable.TABLE_NAME,selection,selectionArgs);
            case NODEINFO_ID:
                selection = DelugeContract.NodeTable._ID + "=?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};
                deleCount = database.delete(DelugeContract.NodeTable.TABLE_NAME,selection,selectionArgs);
                if(deleCount != 0) {
                    getContext().getContentResolver().notifyChange(uri,null);
                }
                return deleCount;
            default:
                throw new IllegalArgumentException("Deletion is not supported for " + uri);
        }
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String selection, @Nullable String[] selectionArgs) {
        final int match = sUriMatcher.match(uri);
        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        int rowsUpdated = 0;
        switch (match) {
            case NODEINFO:
                rowsUpdated = database.update(DelugeContract.NodeTable.TABLE_NAME,contentValues,selection,selectionArgs);
                if(rowsUpdated != 0) {
                    getContext().getContentResolver().notifyChange(uri,null);}
                return rowsUpdated;
            case NODEINFO_ID:
                selection = DelugeContract.NodeTable._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                rowsUpdated = database.update(DelugeContract.NodeTable.TABLE_NAME,contentValues,selection,selectionArgs);
                if(rowsUpdated != 0) {
                    getContext().getContentResolver().notifyChange(uri,null);}
                    return rowsUpdated;

            default:
                throw new IllegalArgumentException("Update not supprted for " + uri);
        }
    }


}
