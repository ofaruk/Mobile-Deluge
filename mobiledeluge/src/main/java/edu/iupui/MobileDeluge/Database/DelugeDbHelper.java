package edu.iupui.MobileDeluge.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.w3c.dom.Node;

import edu.iupui.MobileDeluge.Database.DelugeContract.NodeTable;

/**
 * Created by Omor on 17/07/2017.
 */

public class DelugeDbHelper extends SQLiteOpenHelper {
    public static final String LOG_TAG = DelugeDbHelper.class.getSimpleName();

    //name of the database file
    private static final String DATABASE_NAME = "deluge.db";
    private static final int DATABASE_VERSION = 1;

    /**
     *
     * @param context of the application
     */
    public DelugeDbHelper(Context context) {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    /**
     * This is called when the database is created for the first time
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_NODE_INFO_TABLE = "CREATE TABLE " + NodeTable.TABLE_NAME + " ("
                                        + NodeTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                                        + NodeTable.COLUMN_NODE_ID + " INTEGER NOT NULL UNIQUE, "
                                        + NodeTable.COLUMN_VOLTAGE + " INTEGER NOT NULL, "
                                        + NodeTable.COLUMN_VERSION + " INTEGER NOT NULL, "
                                        + NodeTable.COLUMN_PROGRAMABLE + " TEXT);";
        db.execSQL(CREATE_NODE_INFO_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //nothing to be done since this is the only version
    }
}
