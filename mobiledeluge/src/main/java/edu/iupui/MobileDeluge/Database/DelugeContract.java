package edu.iupui.MobileDeluge.Database;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Omor on 17/07/2017.
 */

public final class DelugeContract {
    //make to constructor private to prevent instantiating the class
    private DelugeContract(){}

    public static final String CONTENT_AUTHORITY = "edu.iupui.MobileDeluge";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_NODEINFO = "nodeinfo";
    public static final String CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_NODEINFO;
    public static final String CONTENT_LIST_ITEM = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_NODEINFO;

    //inner class that defines the table contents. Base columns interface contains the _id column

    public static class NodeTable implements BaseColumns {
        public static final String TABLE_NAME = "nodeinfo";
        public final static Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI,PATH_NODEINFO);

        public static final String COLUMN_NODE_ID ="nodeID";
        public static final String COLUMN_VERSION ="version";
        public static final String COLUMN_VOLTAGE = "voltage";
        public static final String COLUMN_PROGRAMABLE = "programable";
    }
}
