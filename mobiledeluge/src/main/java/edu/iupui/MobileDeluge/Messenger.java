package edu.iupui.MobileDeluge;

/**
 * Created by Omor on 4/09/2016.
 */
public interface Messenger {

    public void message(String s);
}
